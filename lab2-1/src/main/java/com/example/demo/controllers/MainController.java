package com.example.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/index.html")
    public String index() {
        return "index";
    }

    @GetMapping("/rules.html")
    public String rules() {
        return "rules";
    }

    @GetMapping("/game.html")
    public String game() {
        return "game";
    }

}