package lab1;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class MainURLConnection {
    public static void main(String[] args) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("lab1.txt"))) {
            Scanner in = new Scanner(System.in);
            System.out.print("Введите URL: ");
            URL url = new URL(in.nextLine());
            in.close();
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = bufferedReader.readLine();
            while (line != null && !line.isEmpty()) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
            String line1 = bufferedReader.readLine();
            while (line1 != null && !line1.isEmpty()) {
                bufferedWriter.write(line1);
                bufferedWriter.newLine();
                line1 = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
