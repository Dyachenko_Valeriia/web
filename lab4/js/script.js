/*♕*/
const board = [0, 2, 0, 2, 0, 2, 0, 2,
    2, 0, 2, 0, 2, 0, 2, 0,
    0, 2, 0, 2, 0, 2, 0, 2,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 1, 0, 1, 0, 1, 0,
    0, 1, 0, 1, 0, 1, 0, 1,
    1, 0, 1, 0, 1, 0, 1, 0];
const leftboard = [8, 16, 24, 32, 40, 48, 56];
const rightboard = [7, 15, 23, 31, 39, 47, 55];
const upperboard = [0, 1, 2, 3, 4, 5, 6];
const lowerboard = [57, 58, 59, 60, 61, 62, 63];
function example(){
    initialField();
    const board1 = [0, 2, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 2, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 2,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 1, 0, 1,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 4, 0, 0, 0, 0, 0]
    for(i = 0; i < 64; i++){
        board[i] = board1[i];
        if(board1[i] == 2){
            document.getElementById(i).textContent = "⚫";
        }
        if(board1[i] == 1){
            document.getElementById(i).textContent = "⚪";
        }
        if(board1[i] == 0){
            document.getElementById(i).textContent = "";
        }
        if(board1[i] == 4){
            document.getElementById(i).textContent = "♛";
        }
    }
}
function begin(){
    initialField();
    const board1 = [0, 2, 0, 2, 0, 2, 0, 2,
        2, 0, 2, 0, 2, 0, 2, 0,
        0, 2, 0, 2, 0, 2, 0, 2,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 1, 0, 1, 0, 1, 0,
        0, 1, 0, 1, 0, 1, 0, 1,
        1, 0, 1, 0, 1, 0, 1, 0]
    for(i = 0; i < 64; i++){
        board[i] = board1[i];
        if(board1[i] == 2){
            document.getElementById(i).textContent = "⚫";
        }
        if(board1[i] == 1){
            document.getElementById(i).textContent = "⚪";
        }
        if(board1[i] == 0){
            document.getElementById(i).textContent = "";
        }
    }
}
function move(i){
    for(let q = 0; q < 64; q++){
        if(document.getElementById(q).className == "yellow" && q != i){
            return;
        }
    }
    if(board[i] != 0 && document.getElementById(i).className == "dark"){
        document.getElementById(i).className = "yellow";
        const black = checkRedBlack();
        const white = checkRedWhite();
        if((black.size == 0 && (board[i] == 2 || board[i] == 4)) || (white.size == 0 && (board[i] == 1 || board[i] == 3))){
            move1(i);
        }
        else if((board[i] == 2 || board[i] == 4) && black.size != 0){
            if(black.has(i)){
                move1(i);
            }
        }
        else if((board[i] == 1 || board[i] == 3) && white.size != 0){
            if(white.has(i)){
                move1(i);
            }
        }
    }
    else{
        initialField();
    }
}
function move1(i){
    if(board[i] != 0 && document.getElementById(i).className == "dark"){
        document.getElementById(i).className = "yellow";
        if(board[i] == 3 || board[i] == 4){
            king(i);
        }
        else if(!border(i)){
                if(!redElem(i)){
                    greenElem(i);
                }
            }
            if(leftboard.includes(i)){
                let flag = false;
                if(board[i] == 3 || board[i] == 4){
                    leftKing(i);
                }
                if((board[i-7] != 0 && board[i-7] != board[i]) && (board[i-7-7] == 0)) {
                    document.getElementById(i-7-7).className = "red";
                    flag = true;
                }
                if((board[i+9] != 0 && board[i+9] != board[i]) && (board[i+9+9] == 0)) {
                    document.getElementById(i+9+9).className = "red";
                    flag = true;
                }
                if(board[i] == 1 && board[i-7] == 0 && !flag){
                    document.getElementById(i-7).className = "green";
                }
                if(board[i] == 2 && board[i+9] == 0 && !flag){
                    document.getElementById(i+9).className = "green";
                }
            }
            if(rightboard.includes(i)){
                let flag = false;
                if(board[i] == 3 || board[i] == 4){
                    rightKing(i);
                }
                if((board[i+7] != 0 && board[i+7] != board[i]) && (board[i+7+7] == 0)) {
                    document.getElementById(i+7+7).className = "red";
                    flag = true;
                }
                if((board[i-9] != 0 && board[i-9] != board[i]) && (board[i-9-9] == 0)) {
                    document.getElementById(i-9-9).className = "red";
                    flag = true;
                }
                if(board[i] == 2 && board[i+7] == 0 && !flag){
                    document.getElementById(i+7).className = "green";
                }
                if(board[i] == 1 && board[i-9] == 0 && !flag){
                    document.getElementById(i-9).className = "green";
                }
            }
            if(upperboard.includes(i)){
                let flag = false;
                if(board[i] == 3 || board[i] == 4){
                    upperKing(i);
                }
                if((board[i+7] != 0 && board[i+7] != board[i]) && (board[i+7+7] == 0)) {
                    document.getElementById(i+7+7).className = "red";
                    flag = true;
                }
                if((board[i+9] != 0 && board[i+9] != board[i]) && (board[i+9+9] == 0)) {
                    document.getElementById(i+9+9).className = "red";
                    flag = true;
                }
                if((board[i] == 2 || board[i] == 1) && board[i+7] == 0 && !flag){
                    document.getElementById(i+7).className = "green";
                }
                if((board[i] == 2 || board[i] == 1) && board[i+9] == 0 && !flag){
                    document.getElementById(i+9).className = "green";
                }
            }
            if(lowerboard.includes(i)){
                let flag = false;
                if(board[i] == 3 || board[i] == 4){
                    lowerKing(i);
                }
                if((board[i-7] != 0 && board[i-7] != board[i]) && (board[i-7-7] == 0)) {
                    document.getElementById(i-7-7).className = "red";
                    flag = true;
                }
                if((board[i-9] != 0 && board[i-9] != board[i]) && (board[i-9-9] == 0)) {
                    document.getElementById(i-9-9).className = "red";
                    flag = true;
                }
                if((board[i] == 2 || board[i] == 1) && board[i-7] == 0 && !flag){
                    document.getElementById(i-7).className = "green";
                }
                if((board[i] == 2 || board[i] == 1) && board[i-9] == 0 && !flag){
                    document.getElementById(i-9).className = "green";
                }
            }
    } else{
        initialField();
    }
}


function greenElem(i){
    if(board[i] == 1 && board[i-7] == 0){
        document.getElementById(i-7).className = "green";
    }
    if(board[i] == 2 && board[i+7] == 0){
        document.getElementById(i+7).className = "green";
    }
    if(board[i] == 1 && board[i-9] == 0){
        document.getElementById(i-9).className = "green";
    }
    if(board[i] == 2 && board[i+9] == 0){
        document.getElementById(i+9).className = "green";
    }
}

function redElem(i){
    let flag = false;
    if((board[i+7] != 0 && board[i+7] != board[i]) && (!border(i+7) && (board[i+7+7] == 0))) {
        document.getElementById(i+7+7).className = "red";
        flag = true;
    }
    if((board[i-7] != 0 && board[i-7] != board[i]) && (!border(i-7) && (board[i-7-7] == 0))) {
       document.getElementById(i-7-7).className = "red";
       flag = true;
    }
    if((board[i+9] != 0 && board[i+9] != board[i]) && (!border(i+9) && (board[i+9+9] == 0))) {
        document.getElementById(i+9+9).className = "red";
        flag = true;
    }
    if((board[i-9] != 0 && board[i-9] != board[i]) && (!border(i-9) && (board[i-9-9] == 0))) {
        document.getElementById(i-9-9).className = "red";
        flag = true;
    }
    return flag;
}

function initialField(){
    const board2 = [1,3,5,7,
        8,10,12,14,
        17,19,21,23,
        24,26,28,30,
        33,35,37,39,
        40,42,44,46,
        49,51,53,55,
        56,58,60,62];
    for(i = 0; i < 32; i++){
        document.getElementById(board2[i]).className = "dark";
    }
}

function border(i){
    return((leftboard.includes(i) || rightboard.includes(i)) || (upperboard.includes(i) || lowerboard.includes(i)));
}

function king(i){
    let flag = false;
    let flag1 = false;
    let k = i;
    while(!border(i)){
        if(!flag && board[i+7] == 0){
            document.getElementById(i+7).className = "green";
        }
        if(flag1){
            while(board[i+7] == 0 && !border(i)){
                document.getElementById(i+7).className = "red"; 
                i = i+7;
            }
            break;
        }
        if((board[k] == 3 && (board[i+7] == 4 || board[i+7] == 2)) ||
         (board[k] == 4 && (board[i+7] == 3 || board[i+7] == 1)) && board[i+7+7] == 0 && (board[i] != board[i+7])){
            document.getElementById(i+7+7).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i+7] == 3 || board[i+7] == 1)) ||
        (board[k] == 4 && (board[i+7] == 4 || board[i+7] == 2))){
            break;
        }
        i = i + 7;
    }
    i = k;
    flag1 = false;
    while(!border(i)){
        if(!flag && board[i-7] == 0){
            document.getElementById(i-7).className = "green";
        }
        if(flag1){
            while(board[i-7] == 0 && !border(i)){
                document.getElementById(i-7).className = "red"; 
                i = i-7;
            }
            break;
        }
        if((board[k] == 3 && (board[i-7] == 4 || board[i-7] == 2)) ||
         (board[k] == 4 && (board[i-7] == 3 || board[i-7] == 1)) && board[i-7-7] == 0 && (board[i] != board[i-7])){
            document.getElementById(i-7-7).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i-7] == 3 || board[i-7] == 1)) ||
        (board[k] == 4 && (board[i-7] == 4 || board[i-7] == 2))){
            break;
        }
        i = i - 7;
    }
    i = k;
    flag1 = false;
    while(!border(i)){
        if(!flag && board[i+9] == 0){
            document.getElementById(i+9).className = "green";
        }
        if(flag1){
            while(board[i+9] == 0 && !border(i)){
                document.getElementById(i+9).className = "red"; 
                i = i+9;
            }
            break;
        }
        if(((board[k] == 3 && (board[i+9] == 4 || board[i+9] == 2)) ||
          (board[k] == 4 && (board[i+9] == 3 || board[i+9] == 1))) && board[i+9+9] == 0 && (board[i] != board[i+9])){
            document.getElementById(i+9+9).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i+9] == 3 || board[i+9] == 1)) ||
          (board[k] == 4 && (board[i+9] == 4 || board[i+9] == 2))){
            break;
        }
        i = i + 9;
    }
    i = k;
    flag1 = false;
    while(!border(i)){
        if(!flag && board[i-9] == 0){
            document.getElementById(i-9).className = "green";
        }
        if(flag1){
            while(board[i-9] == 0 && !border(i)){
                document.getElementById(i-9).className = "red"; 
                i = i-9;
            }
            break;
        }
        if((board[k] == 3 && (board[i-9] == 4 || board[i-9] == 2)) ||
         (board[k] == 4 && (board[i-9] == 3 || board[i-9] == 1)) && board[i-9-9] == 0 && (board[i] != board[i-9])){
            document.getElementById(i-9-9).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i-9] == 3 || board[i-9] == 1)) ||
        (board[k] == 4 && (board[i-9] == 4 || board[i-9] == 2))){
            break;
        }
        i = i - 9;
    }
    if(flag){
        for(let t=0;t<64;t++){
            if(document.getElementById(t).className == "green"){
                document.getElementById(t).className = "dark";
            }
        }
    }
}

function leftKing(i){
    let flag = false;
    let flag1 = false;
    let k = i;
    while(!(rightboard.includes(i) || upperboard.includes(i) || lowerboard.includes(i))){
        if(!flag && board[i-7] == 0){
            document.getElementById(i-7).className = "green";
        }
        if(flag1){
            while(board[i-7] == 0 && !border(i)){
                document.getElementById(i-7).className = "red"; 
                i = i-7;
            }
            break;
        }
        if((board[k] == 3 && (board[i-7] == 4 || board[i-7] == 2)) ||
         (board[k] == 4 && (board[i-7] == 3 || board[i-7] == 1)) && board[i-7-7] == 0 && (board[i] != board[i-7])){
            document.getElementById(i-7-7).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i-7] == 3 || board[i-7] == 1)) ||
        (board[k] == 4 && (board[i-7] == 4 || board[i-7] == 2))){
            break;
        }
        i = i - 7;
    }
    i = k;
    flag1 = false;
    if(board[i+9] != null){
        while(!(rightboard.includes(i) || upperboard.includes(i) || lowerboard.includes(i))){
            if(!flag && board[i+9] == 0){
                document.getElementById(i+9).className = "green";
            }
            if(flag1){
                while(board[i+9] == 0 && !border(i)){
                    document.getElementById(i+9).className = "red"; 
                    i = i+9;
                }
                break;
            }
            if(((board[k] == 3 && (board[i+9] == 4 || board[i+9] == 2)) ||
                (board[k] == 4 && (board[i+9] == 3 || board[i+9] == 1))) && 
                board[i+9+9] == 0 && (board[i] != board[i+9])){
                document.getElementById(i+9+9).className = "red";
                flag = true;
                flag1 = true;
            }
            if(!flag && (board[k] == 3 && (board[i+9] == 3 || board[i+9] == 1)) ||
                (board[k] == 4 && (board[i+9] == 4 || board[i+9] == 2))){
                break;
            }
            i = i + 9;
        }
    }
    if(flag){
        for(let t=0;t<64;t++){
            if(document.getElementById(t).className == "green"){
                document.getElementById(t).className = "dark";
            }
        }
    }
}

function rightKing(i){
    let flag = false;
    let flag1 = false;
    let k = i;
    while(!(leftboard.includes(i) || upperboard.includes(i) || lowerboard.includes(i))){
        if(!flag && board[i+7] == 0){
            document.getElementById(i+7).className = "green";
        }
        if(flag1){
            while(board[i+7] == 0 && !border(i)){
                document.getElementById(i+7).className = "red"; 
                i = i+7;
            }
            break;
        }
        if((board[k] == 3 && (board[i+7] == 4 || board[i+7] == 2)) ||
         (board[k] == 4 && (board[i+7] == 3 || board[i+7] == 1)) && board[i+7+7] == 0 && (board[i] != board[i+7])){
            document.getElementById(i+7+7).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i+7] == 3 || board[i+7] == 1)) ||
        (board[k] == 4 && (board[i+7] == 4 || board[i+7] == 2))){
            break;
        }
        i = i + 7;
    }
    i = k;
    flag1 = false;
    if(board[i-9] != null){
        while(!(leftboard.includes(i) || upperboard.includes(i) || lowerboard.includes(i))){
            if(!flag && board[i-9] == 0){
                document.getElementById(i-9).className = "green";
            }
            if(flag1){
                while(board[i-9] == 0 && !border(i)){
                    document.getElementById(i-9).className = "red"; 
                    i = i-9;
                }
                break;
            }
            if((board[k] == 3 && (board[i-9] == 4 || board[i-9] == 2)) ||
                (board[k] == 4 && (board[i-9] == 3 || board[i-9] == 1)) &&
                board[i-9-9] == 0 && (board[i] != board[i-9])){
                document.getElementById(i-9-9).className = "red";
                flag = true;
                flag1 = true;
            }
            if(!flag && (board[k] == 3 && (board[i-9] == 3 || board[i-9] == 1)) ||
                (board[k] == 4 && (board[i-9] == 4 || board[i-9] == 2))){
                break;
            }
            i = i - 9;
        }
    }
    if(flag){
        for(let t=0;t<64;t++){
            if(document.getElementById(t).className == "green"){
                document.getElementById(t).className = "dark";
            }
        }
    }
}

function upperKing(i){
    let flag = false;
    let flag1 = false;
    let k = i;
    while(!(rightboard.includes(i) || leftboard.includes(i) || lowerboard.includes(i))){
        if(!flag && board[i+7] == 0){
            document.getElementById(i+7).className = "green";
        }
        if(flag1){
            while(board[i+7] == 0 && !border(i)){
                document.getElementById(i+7).className = "red"; 
                i = i+7;
            }
            break;
        }
        if((board[k] == 3 && (board[i+7] == 4 || board[i+7] == 2)) ||
         (board[k] == 4 && (board[i+7] == 3 || board[i+7] == 1)) && board[i+7+7] == 0 && (board[i] != board[i+7])){
            document.getElementById(i+7+7).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i+7] == 3 || board[i+7] == 1)) ||
        (board[k] == 4 && (board[i+7] == 4 || board[i+7] == 2))){
            break;
        }
        i = i + 7;
    }
    i = k;
    flag1 = false;
    while(!(rightboard.includes(i) || leftboard.includes(i) || lowerboard.includes(i))){
        if(!flag && board[i+9] == 0){
            document.getElementById(i+9).className = "green";
        }
        if(flag1){
            while(board[i+9] == 0 && !border(i)){
                document.getElementById(i+9).className = "red"; 
                i = i+9;
            }
            break;
        }
        if(((board[k] == 3 && (board[i+9] == 4 || board[i+9] == 2)) ||
          (board[k] == 4 && (board[i+9] == 3 || board[i+9] == 1))) && board[i+9+9] == 0 && (board[i] != board[i+9])){
            document.getElementById(i+9+9).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i+9] == 3 || board[i+9] == 1)) ||
          (board[k] == 4 && (board[i+9] == 4 || board[i+9] == 2))){
            break;
        }
        i = i + 9;
    }
    if(flag){
        for(let t=0;t<64;t++){
            if(document.getElementById(t).className == "green"){
                document.getElementById(t).className = "dark";
            }
        }
    }
}

function lowerKing(i){
    let flag = false;
    let flag1 = false;
    let k = i;
    while(!(leftboard.includes(i) || upperboard.includes(i) || rightboard.includes(i))){
        if(!flag && board[i-7] == 0){
            document.getElementById(i-7).className = "green";
        }
        if(flag1){
            while(board[i-7] == 0 && !border(i)){
                document.getElementById(i-7).className = "red"; 
                i = i-7;
            }
            break;
        }
        if((board[k] == 3 && (board[i-7] == 4 || board[i-7] == 2)) ||
         (board[k] == 4 && (board[i-7] == 3 || board[i-7] == 1)) && board[i-7-7] == 0 && (board[i] != board[i-7])){
            document.getElementById(i-7-7).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i-7] == 3 || board[i-7] == 1)) ||
        (board[k] == 4 && (board[i-7] == 4 || board[i-7] == 2))){
            break;
        }
        i = i - 7;
    }
    i = k;
    flag1 = false;
    while(!(leftboard.includes(i) || upperboard.includes(i) || rightboard.includes(i))){
        if(!flag && board[i-9] == 0){
            document.getElementById(i-9).className = "green";
        }
        if(flag1){
            while(board[i-9] == 0 && !border(i)){
                document.getElementById(i-9).className = "red"; 
                i = i-9;
            }
            break;
        }
        if((board[k] == 3 && (board[i-9] == 4 || board[i-9] == 2)) ||
         (board[k] == 4 && (board[i-9] == 3 || board[i-9] == 1)) && board[i-9-9] == 0 && (board[i] != board[i-9])){
            document.getElementById(i-9-9).className = "red";
            flag = true;
            flag1 = true;
        }
        if(!flag && (board[k] == 3 && (board[i-9] == 3 || board[i-9] == 1)) ||
        (board[k] == 4 && (board[i-9] == 4 || board[i-9] == 2))){
            break;
        }
        i = i - 9;
    }
    if(flag){
        for(let t=0;t<64;t++){
            if(document.getElementById(t).className == "green"){
                document.getElementById(t).className = "dark";
            }
        }
    }
}

function checkRedWhite(){
    const whiteNumbers = new Set();
    for(let i = 0; i < 64; i++){
        if(board[i] == 1 || board[i] == 3){
            move1(i);
            for(let j = 0; j < 64; j++){
                if(document.getElementById(j).className == "red"){
                    whiteNumbers.add(i);
                }
            }
            initialField();
        }
    }
    return whiteNumbers;
}
function checkRedBlack(){
    const blackNumbers = new Set();
    for(let i = 0; i < 64; i++){
        if(board[i] == 2 || board[i] == 4){
            move1(i);
            for(let j = 0; j < 64; j++){
                if(document.getElementById(j).className == "red"){
                    blackNumbers.add(i);
                }
            }
            initialField();
        }
    }
    return blackNumbers;
}