package lab1;

import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.util.Scanner;

public class MainSocket {

    public static void main(String[] args) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("lab1.txt"))) {
            String path = "/";
            Scanner in = new Scanner(System.in);
            System.out.print("Введите URL: ");
            URL url = new URL(in.nextLine());
            in.close();
            Socket socket = new Socket(url.getHost(), 80);
            BufferedWriter bufferedWriter1 = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            bufferedWriter1.write("GET "+ path + " HTTP/1.1");
            bufferedWriter1.newLine();
            bufferedWriter1.write("Host: " + url.getHost());
            bufferedWriter1.newLine();
            bufferedWriter1.write("Connection: Close");
            bufferedWriter1.newLine();
            bufferedWriter1.newLine();
            bufferedWriter1.flush();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line = bufferedReader.readLine();
            while (line != null && !line.isEmpty()) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
            String line1 = bufferedReader.readLine();
            while (line1 != null && !line1.isEmpty()) {
                bufferedWriter.write(line1);
                bufferedWriter.newLine();
                line1 = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
